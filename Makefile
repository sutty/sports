v ?= 3.19
m ?= 0
e ?=

# Build using edge
edge ?= false
alpine_version := $(shell echo $(v) | cut -d . -f 1,2)
repository := alpine/v$(alpine_version)/sutty
sources    := sources
apk_cache  := apk-cache
alpine_sdk := sutty/sdk:$(v).$(m)
alpine_builder := gitea.nulo.in/sutty/repository:$(v).$(m)

ifeq ($(edge),true)
alpine_builder := sutty/repository_edge_enabled:latest
endif

packages := $(patsubst ./%/APKBUILD,%,$(wildcard ./*/APKBUILD))

.PHONY: $(packages)
$(packages):
	make all d=$@ edge=$(edge) ; echo -e "\a"

# TODO: Generate the key on the same run and bring it over, then delete
# the container.
alpine/sutty.pub:
	docker run --rm $(alpine_sdk) sh -c 'tar cf - /home/builder/.abuild' | tar --strip-components 2 -xf -
	find .abuild -name "*.pub" | xargs cat > $@
	test -s $@

list: ## List all packages
	@echo $(packages) | tr " " "\n"

all: build release ## Build and release

# Create the directory
$(repository):
	mkdir -p $@

pubkey: alpine/sutty.pub ## Get public key

create_directories := $(sources) $(apk_cache) $(HOME)/.ccache
$(create_directories):
	mkdir -p $@

build: $(repository) $(create_directories) ## Compile the package in an Alpine container
	docker run --rm \
		-v $(PWD)/$(repository):/home/builder/packages/builder \
		-v $(PWD)/$(d):/home/builder/package \
		-v $(PWD)/$(sources):/var/cache/distfiles \
		-v $(PWD)/.abuild:/home/builder/.abuild \
		-v $(HOME)/.ccache:/home/builder/.ccache \
		-v $(PWD)/$(apk_cache):/etc/apk/cache \
		-it $(alpine_builder)

release: pubkey ## Upload packages
	rsync -av --delete-after --chown 1000:82 alpine/v$v/ root@athshe.sutty.nl:/srv/sutty/srv/http/data/_deploy/alpine.sutty.nl/alpine/v$v/
	rsync -av --delete-after --chown 1000:82 alpine/v$v/ root@anarres.sutty.nl:/srv/sutty/srv/http/data/_deploy/alpine.sutty.nl/alpine/v$v/
	rsync -av --delete-after --chown 1000:82 alpine/v$v/ root@gethen.sutty.nl:/srv/sutty/srv/panel.sutty.nl/_deploy/alpine.sutty.nl/alpine/v$v/

pull: ## Pull repo
	mkdir -p alpine/v$v/
	umask 027 ; rsync -rv root@gethen.sutty.nl:/srv/sutty/srv/panel.sutty.nl/_deploy/alpine.sutty.nl/alpine/v$v/ alpine/v$v/
