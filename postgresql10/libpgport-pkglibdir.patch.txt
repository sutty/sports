Patch-Source: https://sources.debian.org/src/postgresql-14/14.0-1/debian/patches/libpgport-pkglibdir
Author: Christoph Berg <myon@debian.org>
Description: Move libpgport/libpgcommon/libpgfeutils from libdir to pkglibdir
 This allows client applications to link to version-specific libraries.
 Used by pg-checksums.

--- a/src/fe_utils/Makefile
+++ b/src/fe_utils/Makefile
@@ -35,13 +35,13 @@ distprep: psqlscan.c
 
 # libpgfeutils could be useful to contrib, so install it
 install: all installdirs
-	$(INSTALL_STLIB) libpgfeutils.a '$(DESTDIR)$(libdir)/libpgfeutils.a'
+	$(INSTALL_STLIB) libpgfeutils.a '$(DESTDIR)$(pkglibdir)/libpgfeutils.a'
 
 installdirs:
-	$(MKDIR_P) '$(DESTDIR)$(libdir)'
+	$(MKDIR_P) '$(DESTDIR)$(pkglibdir)'
 
 uninstall:
-	rm -f '$(DESTDIR)$(libdir)/libpgfeutils.a'
+	rm -f '$(DESTDIR)$(pkglibdir)/libpgfeutils.a'
 
 clean distclean:
 	rm -f libpgfeutils.a $(OBJS) lex.backup
--- a/src/common/Makefile	2022-09-03 16:18:57.242768946 +0000
+++ b/src/common/Makefile	2022-09-03 16:21:05.852444039 +0000
@@ -59,13 +59,13 @@
 
 # libpgcommon is needed by some contrib
 install: all installdirs
-	$(INSTALL_STLIB) libpgcommon.a '$(DESTDIR)$(libdir)/libpgcommon.a'
+	$(INSTALL_STLIB) libpgcommon.a '$(DESTDIR)$(pkglibdir)/libpgcommon.a'
 
 installdirs:
-	$(MKDIR_P) '$(DESTDIR)$(libdir)'
+	$(MKDIR_P) '$(DESTDIR)$(pkglibdir)'
 
 uninstall:
-	rm -f '$(DESTDIR)$(libdir)/libpgcommon.a'
+	rm -f '$(DESTDIR)$(pkglibdir)/libpgcommon.a'
 
 libpgcommon.a: $(OBJS_FRONTEND)
 	rm -f $@
--- a/src/port/Makefile	2022-09-03 16:21:56.898345718 +0000
+++ b/src/port/Makefile	2022-09-03 16:22:19.426536947 +0000
@@ -46,13 +46,13 @@
 
 # libpgport is needed by some contrib
 install: all installdirs
-	$(INSTALL_STLIB) libpgport.a '$(DESTDIR)$(libdir)/libpgport.a'
+	$(INSTALL_STLIB) libpgport.a '$(DESTDIR)$(pkglibdir)/libpgport.a'
 
 installdirs:
-	$(MKDIR_P) '$(DESTDIR)$(libdir)'
+	$(MKDIR_P) '$(DESTDIR)$(pkglibdir)'
 
 uninstall:
-	rm -f '$(DESTDIR)$(libdir)/libpgport.a'
+	rm -f '$(DESTDIR)$(pkglibdir)/libpgport.a'
 
 libpgport.a: $(OBJS)
 	rm -f $@
--- a/src/Makefile.global.in	2022-09-03 16:24:41.165156257 +0000
+++ b/src/Makefile.global.in	2022-09-03 16:25:54.782578229 +0000
@@ -534,7 +534,7 @@
 # pgport before libpq.  This does cause duplicate -lpgport's to appear
 # on client link lines.
 ifdef PGXS
-libpq_pgport = -L$(libdir) -lpgcommon -lpgport $(libpq)
+libpq_pgport = -L$(pkglibdir) -lpgcommon -lpgport $(libpq)
 else
 libpq_pgport = -L$(top_builddir)/src/common -lpgcommon -L$(top_builddir)/src/port -lpgport $(libpq)
 endif
