# Sutty ports

Packages for [Alpine Linux](https://alpinelinux.org/)

[Read more](https://alpine.sutty.nl/en/)
([Castellano](https://alpine.sutty.nl/))

## Dependencies

You need to have [Docker](https://docker.com) and Make installed.

## Pull keys

To pull the keys to sign and upload packages, copy the `alpine.sutty.nl` secret from our password manager then extract it using:

```bash
# If using Wayland
wl-paste | base64 -d | tar -zvx
# or if you're using X11
xsel -ob | base64 -d | tar -zvx
```

## Pull packages

To update the repo and upload the changes, you'll need to pull the current repo to later generate the apropiate `APKINDEX`:

```bash
make pull
```

## Build packages

Run the builder using `make` and the package name as argument:

```bash
make package
```

This will build the package and if everything goes ok it will
synchronize changes to the <https://alpine.sutty.nl> site.  You'll need
access of course!

## Add a package

Create a directory with the package name (not required to be the same
but why invent another?) and place an APKBUILD and other build files
inside it.
