# Maintainer: f <f@sutty.nl>
pkgname=postgrest
pkgver=10.1.1
pkgrel=0
pkgdesc="REST API for any Postgres database"
url="https://postgrest.org"
# limited by ghc
arch="aarch64 x86_64"
license="MIT"
makedepends="ghc cabal zlib-dev libpq-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/PostgREST/postgrest/archive/refs/tags/v${pkgver}.tar.gz"
options="net !check"

# Cabal seems to be built without sandbox, moving the cabal-dir into src
export CABAL_DIR="$srcdir/.cabal"

build() {
  export PATH="$PATH:/usr/lib/llvm14/bin"
  cabal update
  cabal install --only-dependencies
  cabal configure \
    --prefix='/usr' \
    --enable-tests \
    --enable-split-sections \
  cabal build --jobs=${JOBS:-1}
}

package() {
  _bindir="$pkgdir/usr/bin"
  mkdir -p "$_bindir"
  cabal install \
    --installdir="$_bindir" \
    --install-method=copy
}
sha512sums="
69aa9d53c836880693d5fd61904143b21b8656dc12b99416bd35ffddd6904d40ac62e162c67373c89bde45476de906733057c07064b3c41af496a98cdd2a233a  postgrest-10.1.1.tar.gz
"
